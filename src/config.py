import json
import logging
import sys
import traceback
from pathlib import Path
from typing import TextIO


class Config:
    """
    Contains all the configuration variables.
    """
    # Options
    # Logs
    log_exceptions = False
    log_mode = logging.DEBUG
    # Customize
    customize_app_names = False
    rootdir_resources = "rec"


class Runtime:
    """
    Contains all relevant runtime variables.
    """
    apps = {}
    profiles = []


def choose_log_mode(log_mode):
    if log_mode == "debug":
        return logging.DEBUG
    elif log_mode == "info":
        return logging.INFO
    elif log_mode == "warning":
        return logging.WARNING
    elif log_mode == "error":
        return logging.ERROR
    elif log_mode == "CRITICAL":
        return logging.CRITICAL
    else:
        input("The log_mode is configured wrong!")
        raise ValueError("Check the config.json for misconfiguration of the log_mode!")


def get_json_file(filename: str, mode="r") -> TextIO:
    """
    Loading a json file that is located in the "rootdir_resources"-folder.
    :param filename: name of the *.json file
    :param mode: mode of open()
    :return: return file
    """
    if not Path("{}".format(Config.rootdir_resources)).exists():
        Path("{}".format(Config.rootdir_resources)).mkdir()

    try:
        f = open(Path(r".\{}\{}.json".format(Config.rootdir_resources, filename)),
                 mode,
                 encoding="utf8")
        logging.debug("File loaded: <{}>".format(f.name))
        return f
    except FileNotFoundError:
        logging.debug(".json not found (first time call)")
        f = open(Path(r".\{}\{}.json".format(Config.rootdir_resources, filename)),
                 mode="w+",
                 encoding="utf8")
        f.write("[]")
        logging.info(".json created successfully")
        return f


def check_if_json_is_empty(file) -> bool:
    """
    True: if file is empty.
    False if file is filled.
    """
    check = file.read() == ''
    file.seek(0)
    return check


def set_exeptions_to_logs():
    """
    This method sets all exceptions to the log-stream.
    """

    def __log_except_hook(*exc_info):
        text = "".join(traceback.format_exception(*exc_info))
        logging.critical("Unhandled exception: %s", text)

    sys.excepthook = __log_except_hook


def init_logs():
    """Initiates the logging module"""
    for handler in logging.root.handlers[:]:
        logging.root.removeHandler(handler)
    logging.basicConfig(filename=Path("{}/debug.log".format(Config.rootdir_resources)).absolute(),
                        level=logging.DEBUG,
                        format="%(levelname)s[%(asctime)s]: %(message)s",
                        datefmt="%d.%m.%Y %H:%M:%S")
    logging.info("Started Application; log_mode={}".format(Config.log_mode))
    if Config.log_exceptions:
        set_exeptions_to_logs()


def load_config():
    """
    Loads the config.json and puts everything into the Config class.
    If the config.json does not exist, create a new one with default settings.
    """
    json_file = get_json_file("config")
    if check_if_json_is_empty(json_file):
        config_dict = {
            "log_exceptions": True,
            "customize_app_names": False,
            "log_mode": "info"
        }
        json.dump(config_dict, json_file)
    else:
        config_dict = json.loads(json_file.read())
        Config.log_exceptions = config_dict["log_exceptions"]
        Config.customize_app_names = config_dict["customize_app_names"]
        Config.log_mode = choose_log_mode(config_dict["log_mode"])
    json_file.close()
