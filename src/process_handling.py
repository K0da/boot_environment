import psutil as ps


def get_unique_user_processes():
    """
    checks for every process that is assigned to a user and gets rid of all duplicate named processes
    :return: list of all unique named processes
    """
    processes = ps.process_iter(['name', 'username'])
    already_checked_exe = []
    unique_user_processes = []
    for i in processes:
        if i.info["username"] is not None:
            if i.info["name"] not in already_checked_exe:
                already_checked_exe.append(i.info["name"])
                unique_user_processes.append(i)
            else:
                continue
    unique_user_processes.remove(ps.Process(0))  # remove "System Idle Process" because of access problems
    return unique_user_processes


def make_dict(processes: list) -> dict:
    """
    creates a dict out of a list of processes
    :param processes: list of ps.Process-es
    :return: dict -> {i.pid: [i.name(), i.username(), i.exe()]}
    """
    processes_dict = dict()
    for i in processes:
        i: ps.Process
        processes_dict[i.pid] = [i.name(), i.username(), i.exe()]
    return processes_dict


def count_unique_user_processes():
    """
    :return: number of all unique (by name) user processes
    """
    return len([x.info for x in get_unique_user_processes()])


def print_ps_id_and_name(processes: list):
    human_readable_list = dict()
    for i in processes:
        i: ps.Process
        human_readable_list[i.pid] = i.name()
    print(human_readable_list.values())


def register() -> dict:
    """
    Looks for apps that the user might want to register. This takes 3 steps.
    1. save all current processes
    2. save all current processes after user opened desired applications
    3. remove all processes that were not running at [1.]
    :return: dict of processes user wants to register
    """

    print("We are going to register a new application. \n"
          "Please close all applications you want to register now!")
    check_ready()
    unwanted_processes = set(get_unique_user_processes())
    print("Please only open the apps you want to register now!")
    check_ready()
    wanted_processes = set(get_unique_user_processes())
    new_processes = unwanted_processes.intersection(wanted_processes)
    processes_dict = make_dict(get_unique_user_processes())
    new_processes = make_dict(list(new_processes))
    for p_id in new_processes.keys():
        del processes_dict[p_id]
    return processes_dict


def check_ready():
    """
    Waits for an user input to resume.
    """
    while True:
        if input("If you are ready enter [y] to continue!").casefold() == "y":
            break


def testing():
    """Just some bulk that I used to test..."""
    print_ps_id_and_name(get_unique_user_processes())
    print()
    print(make_dict(get_unique_user_processes()))
    print()
    print(count_unique_user_processes())


if __name__ == "__main__":
    # testing()
    register()
