import logging


def register_app():
    """Register a new app by looking up processes."""
    from src import process_handling
    from src import config as conf
    from src.command_line import CommandLine
    from src.App import App, save_all_apps

    new_apps = process_handling.register()
    print("I found {} applications.".format(len(new_apps)))
    if conf.Config.customize_app_names:
        # set custom app name
        pass
    else:
        for i in new_apps.values():
            print("I found {}.\n "
                  "Do you want to register? This application?".format(i[0]))
            while True:
                usr_in = CommandLine.get_user_input("[Y/n]")
                if usr_in == "y" or usr_in == "":
                    conf.Runtime.apps.append(App(i[0], i[2]))
                    break
                elif usr_in == "n":
                    break
                else:
                    print('Please make your decision clear by entering "y" or "n".')
    save_all_apps(conf.Runtime.apps)


def load_apps_from_json():
    """Loads all saved apps from the apps.json and loads them into the Runtime instance"""
    from src.App import load_all_apps
    loaded_apps = load_all_apps()
    if len(loaded_apps) != 0:
        from src.config import Runtime
        Runtime.apps.update(loaded_apps)


def human_readable_profile_list(profiles: list) -> str:
    info = ""
    profile_number = 0
    for i in profiles:
        profile_number += 1
        info + " <{}>[{}] | ".format(profile_number, i.name)
    return info


def start_profile(number: int):
    from src.config import Runtime
    chosen_profile = Runtime.profiles[number - 1]
    for app in chosen_profile.apps:
        app.run()
    print("Staring Profile '{}'.".format(chosen_profile.name))


def get_all_apps_names(apps: list) -> str:
    """
    Goes through a list of apps. Collects every apps name and adds
    all names together into a big string.
    :param apps: list of <App>s
    :return: string with all names
    """
    app_names = str()
    for i in apps:
        app_names = app_names + "<{}> ".format(i.name)
    return app_names


def create_new_profile():
    """
    User interface -> User can create a new Profile
    """
    from src.config import Runtime
    from src.App import App
    from src.command_line import CommandLine
    from src.Profile import Profile, save_all_profiles
    apps_list = list(Runtime.apps.values())
    print('\n* Creating Profile *')
    print('Give your Profile a new name:')
    user_input = input('> ')
    new_profile = Profile(user_input)
    print("These are the registered apps you can use: ")
    print('When done type "exit" or "done."')
    while True:
        app_nr = 0
        for i in apps_list:
            i: App
            app_nr += 1
            print("<{}> [{}], ".format(app_nr, i.name), end='')
        user_input = CommandLine.get_user_input("\nChoose one App by typing one number :")
        if user_input in ["e", "exit", "done", "d"]:
            break
        else:
            try:
                new_profile.set_new_app(apps_list.pop(int(user_input) - 1))
                logging.debug("Profile: {}".format(str(new_profile)))
            except ValueError:
                print("Please enter a number!")
            except IndexError:
                print("You do not have that many apps dude! You only have {}!".format(app_nr))
            finally:
                print("You have chosen these Apps: [{}]".format(get_all_apps_names(new_profile.apps)))
    print('New Profile "{}" created.'.format(new_profile.name))
    Runtime.profiles.append(new_profile)
    save_all_profiles()
