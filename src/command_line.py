from src import manage


class CommandLine:
    menu_state = str

    @staticmethod
    def get_user_input(message=""):
        """
        takes user input
        :param message: will be displayed when input is called
        :return: string of user-input
        """
        if message == "":
            user_input = input("> ").casefold()
        else:
            user_input = input("{}\n> ".format(message)).casefold()
        return user_input

    def welcome(self):
        from src.config import Runtime
        from src.manage import human_readable_profile_list, start_profile
        self.menu_state = "run"
        while True:
            print("\n\n*** Welcome User ***")
            print('For help just type "help"')
            print("{} Profiles loaded | {} Apps loaded".format(len(Runtime.profiles), len(Runtime.apps)))
            print("Available Profiles: {}".format(human_readable_profile_list(Runtime.profiles)))
            user_input = self.get_user_input()
            if user_input in ["h", "help"]:
                self.msg_help()
            elif user_input.isdigit():
                start_profile(int(user_input))
            elif user_input in ["s", "settings"]:
                self.settings()
            elif user_input in ["e", "exit", "x"]:
                break

    def settings(self):
        self.menu_state = "settings"
        print("\n** Settings **")
        while True:
            user_input = self.get_user_input()
            if user_input in ["h", "help"]:
                self.msg_help()
            elif user_input in ["a", "app"]:
                manage.register_app()
            elif user_input in ["p", "profile"]:
                manage.create_new_profile()
            elif user_input in ["e", "exit", "x"]:
                break

    def msg_help(self):
        """Displays a list of available commands"""
        # Main Menu
        if self.menu_state == "run":
            print("*** Help ***")
            print("Style of display: <Number of Profile>[Name of profile]\n"
                  "To start a profile just type the provided number")
            print("help         displays this help\n"
                  "settings     go to Settings Menu\n"
                  "list         show all available profiles\n"
                  "exit         exit this application")
        # New Menu
        elif self.menu_state == "settings":
            print("\n** Help [Settings] **")
            print("Here you can create edit things: ")
            print("edit <Profilename>   edit a profile\n"
                  "app                  register a edit app\n"
                  "profile              create a edit profile")
        else:
            raise RuntimeError("This menu does not exist. You went off the grid!")

    def run(self):
        self.welcome()


if __name__ == "__main__":
    cmd = CommandLine()
    cmd.run()
