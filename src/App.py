import json
import logging
import os
from pathlib import Path


class App:
    def __init__(self, name: str, path_exec, custom_name=None):
        """
        This class conatins all information about the app you want to use.
        :param name: name of the Application
        :param custom_name: if name set manually insert here
        :param path_exec: global path to the .exe of the app
        """
        self.name_raw = name
        self.path_obj = Path(path_exec)
        self.path_raw = path_exec
        if custom_name is None:
            if name.endswith(".exe"):
                self.name = name[:-4]
            else:
                self.name = name
                logging.debug("No [.exe] found in App name")
        else:
            self.name = custom_name

    def __str__(self):
        return "App[name: <{}>, name_raw: <{}>, path_raw: <{}>, path_obj: <{}>]".format(
            self.name, self.name_raw, self.path_raw, self.path_obj.__str__()
        )

    def run(self):
        os.startfile(os.path.normpath("{}".format(self.path_raw)))
        print("Running [{}]!".format(self.name))


def load_all_apps() -> dict:
    from src.config import get_json_file
    file = get_json_file(filename="apps")
    loaded_apps = {}
    json_apps = json.loads(file.read())
    for i in json_apps:
        loaded_apps[i["name_raw"]] = App(i["name_raw"],
                                         i["path_raw"],
                                         i["name"])
        logging.debug("App loaded: {}".format(str(i)))
    file.close()
    return loaded_apps


def save_all_apps(apps: dict):
    from src.config import get_json_file
    file = get_json_file(filename="apps", mode="w")
    json_apps = []
    for app in apps.values():
        app_dict = {
            "name_raw": app.name_raw,
            "path_raw": app.path_raw,
            "name": app.name,
        }
        json_apps.append(app_dict)
    file.write(json.dumps(json_apps))
    logging.info("Apps saved to .json")
    file.close()
