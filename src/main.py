from src.config import init_logs, load_config, Config, Runtime
from src.command_line import CommandLine
from src import manage
from src.App import save_all_apps
from src.Profile import save_all_profiles

Config()
Runtime()


def run():
    load_config()
    init_logs()
    manage.load_apps_from_json()
    cl = CommandLine()
    cl.run()
    save_all_apps(Runtime.apps)
    save_all_profiles()
