import json
import logging

from src.config import Runtime


class Profile:

    def __init__(self, name: str):
        self.name = name
        self.apps = []
        # Increase counter

    def __str__(self):
        return "Profile({}, {})".format(self.name, [x.__str__() for x in self.apps])

    def set_new_app(self, apps: list):
        self.apps.append(apps)


def load_all_profiles():
    from src.config import get_json_file
    file = get_json_file(filename="profiles")
    loaded_profiles = []
    json_profiles = json.loads(file.read())
    for i in json_profiles:
        new_profile = Profile(i["name"])
        for app in json_profiles["app_names"]:
            new_profile.set_new_app(app)
        loaded_profiles.append(new_profile)
    Runtime.profiles.append(loaded_profiles)
    file.close()


def save_all_profiles():
    from src.config import get_json_file
    file = get_json_file(filename="profiles", mode="w")
    json_profiles = []
    profiles_runtime = Runtime.profiles
    for profile in profiles_runtime:
        profiles_dict = {
            "name": profile.name,
            "app_names": [i.apps.name_raw for i in profile]
        }
        json_profiles.append(profiles_dict)
    file.write(json.dumps(json_profiles))
    logging.info("Profiles saved to .json")
    file.close()
